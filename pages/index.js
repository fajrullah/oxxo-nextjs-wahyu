import React, { Component } from 'react'
import LayoutMain from '../components/LayoutMain'
import Link from 'next/link'
import axios from 'axios'

class Homepage extends Component {

  state = {
    email: '',
    phone: '',
    name: '',
    content: ''
  }

  handleSubmit = event => {
    event.preventDefault();

    const posts = {
      body: {
        data: {
          email: this.state.email, 
          phone: this.state.phone, 
          name: this.state.name, 
          content: this.state.content
        }
      }
    };

    const Contentheaders = {
      // 'Accept': 'application/json',
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiOTNkZGY2OTY4ZDZkMDE4NzM4YzFhZGE3NzhmN2U0ZWZhMWFkNTI0NzYyZDNjMjU0OWFjYmM0ZjJjNGY3N2E0Njg1MDY1ZDQzNDRhZjRhZmMiLCJpYXQiOjE1OTEyODUyNTEsIm5iZiI6MTU5MTI4NTI1MSwiZXhwIjoxNjIyODIxMjUxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.XOalehEFeCIPoBhNIYSupRDAjxvXa2CwcP8k7m-_Y6F5YUEnGLOFPx8TV3s6yfu8Yyqpqm6tMcjHu-BH6cRGCX5otqsWvi50mwFvkiylq5G2MYEvGHfk_3AlDNtyHOznG_HjFTn-jgPEc3rKbkxQNUwgL26xUdAN06al2g4QosZ82MvhbTG8B3oqq-laFy3faehfzLcH0ybkeSxUdlAYyARzUVvE7H05OWu2hOoFuzaGtoS0W2liMgVqLxc-lOWs2s1gA8YRESZGBLYCemi0p6kFebUa6N1g-DQOpMu2y5o35q8euySs9aEXJf-DxBGSn6_CDQt1LtKQ1w7f2F31xfIIut2nIeANbPUw_febP3x_I_uaxL-0lGOg7dkDSxPvvMnv6S2gwZmDMeeWPOejwuU2hWMYm4dqzYNv7_tYGrBZykHykt5Jkvh0gV_Y3P3QE5TzsJAf40F8ddqBFwgcf3Uh79q6UDtleZaqspKALZG4XwdDoF3acba7RgSLNNjPEtLMS_rMvvEvoMalESp1JUXMJbyUpDSMuUa9Gj1dMfWkBYyf5P2r_Enip9p6CTsLs2vjhdtViqtRPRrigzCC5whRFxawn1s_Zy4gTJKffzUqZCwfY6gAmIoZth2X42dvqrr-arnnd0VqSIWkd8XI7p4virlSgSOOH85rWU2BGvY',
      'Content-Type': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Credentials': 'true',
      // 'Content-Type': 'application/json;charset=UTF-8',
    }

    axios.post('http://18.140.63.69:80/contact/1',posts, {headers: Contentheaders })
      .then(res => {
        console.log(res.data, "Data");
      })
  }

  

  render() {

    const homePage = {
      website: "javascript:void(0)",
    };
    
    return (
      <LayoutMain title='Home'>
        <div data-offset="90" data-spy="scroll" data-target=".navbar">
          {/* Preloader */}
          <div className="preloader">
            <div className="box" />
          </div>
          {/* Ink Transition */}
          <div className="cd-transition-layer visible opening">
            <div className="bg-layer" />
          </div>
          {/*/Preloader */}
          {/*Header Start*/}
          <header className="cursor-light">
            {/*Navigation*/}
            <nav className="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon alt-font">
              <div className="container">
                <a className="logo link scroll" href={homePage.website} title="Logo">
                  {/*Logo Default*/}
                  <img alt="logo" className="logo-dark default" src="../static/assets/img/webp/logo-white.webp" />
                </a>
                {/*Nav Links*/}
                <div className="collapse navbar-collapse" id="agency">
                  <div className="navbar-nav ml-auto">
                    <a className="nav-link link scroll active" href="#home">
                      Home
                    </a>
                    <a className="nav-link link scroll" href="#about-us">
                      About Us
                    </a>
                    <a className="nav-link link scroll" href="#portfolio">
                      Our Work
                    </a>
                    <a className="nav-link link scroll" href="#clients">
                      Clients
                    </a>
                    <a className="nav-link link scroll" href="#blog">
                      Our Blog
                    </a>
                    <a className="nav-link link scroll" href="#contact">
                      Contact Us
                    </a>
                    <span className="menu-line">
                      <i aria-hidden="true" className="fa fa-angle-down" />
                    </span>
                  </div>
                  <a className="btn btn-medium btn-rounded btn-transparent-white btn-hvr-white ml-3"
                    data-animation-duration={500} data-fancybox data-src="#animatedModal" href={homePage.website}>
                    Boost Your Business
                    <div className="btn-hvr-setting">
                      <ul className="btn-hvr-setting-inner">
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                      </ul>
                    </div>
                  </a>
                </div>
                {/*Menu Button*/}
                <button className="fullnav-toggler link" id="full-menu-1" type="button">
                  <span className="line" />
                  <span className="line" />
                  <span className="line" />
                </button>
                {/*Slider Social*/}
                <div className="slider-social">
                  <ul className="list-unstyled">
                    <li className="animated-wrap">
                      <a className="animated-element" href={homePage.website}>
                        <i aria-hidden="true" className="fab fa-facebook-f" />
                      </a>
                    </li>
                    <li className="animated-wrap">
                      <a className="animated-element" href={homePage.website}>
                        <i aria-hidden="true" className="fab fa-instagram" />
                      </a>
                    </li>
                    <li className="animated-wrap">
                      <a className="animated-element" href={homePage.website}>
                        <i aria-hidden="true" className="fab fa-twitter" />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            {/*Full menu*/}
            <div className="nav-holder main style-2 alt-font">
              {/*Menu Button*/}
              <button className="fullnav-close link" type="button">
                <span className="line" />
                <span className="line" />
                <span className="line" />
              </button>
              <div className="container">
                <div className="shape-left">
                  <nav className="navbar full-menu-navigation left">
                    <ul className="list-unstyled">
                      <li>
                        <a className="link scroll" href="#home">
                          <span className="anchor-circle" />
                          <span className="anchor-text">Home</span>
                        </a>
                      </li>
                      <li>
                        <a className="link scroll" href="#about-us">
                          <span className="anchor-circle" />
                          <span className="anchor-text">About</span>
                        </a>
                      </li>
                      <li>
                        <a className="link scroll" href="#portfolio">
                          <span className="anchor-circle" />
                          <span className="anchor-text">Work</span>
                        </a>
                      </li>
                      <li>
                        <a className="link scroll" href="#clients">
                          <span className="anchor-circle" />
                          <span className="anchor-text">Clients</span>
                        </a>
                      </li>
                      <li>
                        <a className="link scroll" href="#blog">
                          <span className="anchor-circle" />
                          <span className="anchor-text">Blog</span>
                        </a>
                      </li>
                      <li>
                        <a className="link scroll" href="#contact">
                          <span className="anchor-circle" />
                          <span className="anchor-text">Contact</span>
                        </a>
                      </li>
                    </ul>
                    <span className="full-menu-dot" style={{ transform: "scale(0)" }} />
                  </nav>
                  <img alt="shape" src="../static/assets/img/webp/shape-8.webp" />
                </div>
                <div className="shape-right">
                  <div className="full-menu-detail hide-cursor">
                    <h6 className="title">Press Contact</h6>
                    <p>
                      <i className="fas fa-user-alt" />
                      David Warrior
                    </p>
                    <p>
                      <i className="fas fa-mobile-alt" />
                      +97 53 49 24 78 36
                    </p>
                    <p>
                      <i className="fas fa-envelope" />
                      contact@Oxxo.com
                    </p>
                  </div>
                  <img alt="shape" src="../static/assets/img/webp/shape-7.webp" />
                </div>
              </div>
            </div>
            {/*Get Quote Model Popup*/}
            <div className="animated-modal hidden quote-content" id="animatedModal">
              {/*Heading*/}
              <div className="heading-area pb-2 mx-570">
                <span className="sub-title">We are Oxxo company</span>
                <h2 className="title mt-2">
                  Lets start your{" "}
                  <span className="alt-color js-rotating">project, website</span>
                </h2>
              </div>
              {/*Contact Form*/} 
              <form onSubmit={this.handleSubmit} className="contact-form">
                <div className="row">
                  {/*Result*/}
                  <div className="col-12" id="quote_result" />
                  {/*Left Column*/}
                  <div className="col-md-6">
                    <div className="form-group">
                      <input className="form-control" onChange={(event) => this.setState({name: event.target.value})} name="name" placeholder="Full Name" required
                        type="text" />
                    </div>
                    <div className="form-group">
                      <input className="form-control" onChange={(event) => this.setState({phone: event.target.value})} name="phone" placeholder="Contact Phone, ex: 082274586011"
                        required type="text" />
                    </div>
                  </div>
                  {/*Right Column*/}
                  <div className="col-md-6">
                    <div className="form-group">
                      <input className="form-control" onChange={(event) => this.setState({email: event.target.value})} name="email" placeholder="Email" required
                        type="email" />
                    </div>
                  </div>
                  {/*Full Column*/}
                  <div className="col-md-12">
                    <div className="form-group">
                      <textarea className="form-control" onChange={(event) => this.setState({content: event.target.value})} name="content"
                        placeholder="Please explain your project in detail." required />
                    </div>
                  </div>
                  {/*Button*/}
                  <div className="col-md-12">
                    {/* <div className="form-check">
                      <label className="checkbox-lable">
                        Contact by phone ins preffered
                        <input type="checkbox" />
                        <span className="checkmark" />
                      </label>
                    </div> */}
              <button
                className="btn btn-large btn-rounded btn-blue btn-hvr-pink mt-4 text-white"
                type="submit"
              >
                Send Message
                <div className="btn-hvr-setting">
                  <ul className="btn-hvr-setting-inner">
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                  </ul>
                </div>
              </button>
            </div>
          </div>
        </form>
      </div>
    </header>
    {/*Header End*/}
    {/*Slider Start*/}
    <section className="p-0 no-transition cursor-light" id="home">
      <h2 className="d-none">hidden</h2>
      <div
        className="rev_slider_wrapper fullscreen-container"
        data-alias="Oxxo-agency-1"
        data-source="gallery"
        id="rev_slider_17_1_wrapper"
        style={{ background: "transparent", padding: 0 }}
      >
        {/* START REVOLUTION SLIDER 5.4.8.1 fullscreen mode */}
        <div
          className="rev_slider fullscreenbanner"
          data-version="5.4.8.1"
          id="rev_slider_17_1"
          style={{ display: "none" }}
        >
          <ul>
            {" "}
            {/* SLIDE  */}
            <li
              data-description
              data-easein="default"
              data-easeout="default"
              data-hideafterloop={0}
              data-hideslideonmobile="off"
              data-index="rs-43"
              data-masterspeed="default"
              data-param1
              data-param10
              data-param2
              data-param3
              data-param4
              data-param5
              data-param6
              data-param7
              data-param8
              data-param9
              data-rotate={0}
              data-saveperformance="off"
              data-slotamount="default"
              data-title="Slide"
              data-transition="fade"
            >
              {/*OVERLAY*/}
              <div className="gradient-bg1 bg-overlay" />
              {/* LAYER NR. 3 */}
              <div
                className="tp-caption   tp-resizeme"
                data-frames='[{"delay":220,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;rZ:339;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['-26','-26','83','83']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['98','98','60','60']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['left','left','left','left']"
                data-y="['bottom','bottom','bottom','bottom']"
                id="slide-43-layer-5"
                style={{ zIndex: 7 }}
              >
                <div
                  className="rs-looped rs-slideloop"
                  data-easing
                  data-speed={5}
                  data-xe={0}
                  data-xs={0}
                  data-ye={15}
                  data-ys={-15}
                >
                  <img
                    alt="image"
                    data-hh="['87px','87px','87px','87px']"
                    data-no-retina
                    data-ww="['44px','44px','44px','44px']"
                    src="../static/assets/img/webp/shape-6.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 4 */}
              <div
                className="tp-caption   tp-resizeme"
                data-basealign="slide"
                data-frames='[{"delay":210,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;rZ:358;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['700','700','700','530']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['148','148','148','148']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-6"
                style={{ zIndex: 8 }}
              >
                <div
                  className="rs-looped rs-slideloop"
                  data-easing="Power0.easeIn"
                  data-speed={5}
                  data-xe={0}
                  data-xs={0}
                  data-ye={20}
                  data-ys={0}
                >
                  <img
                    alt="image"
                    data-hh="['87px','87px','87px','87px']"
                    data-no-retina
                    data-ww="['24px','24px','24px','24px']"
                    src="../static/assets/img/webp/shape-5.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 5 */}
              <div
                className="tp-caption   tp-resizeme"
                data-fontsize="['20','20','20','20']"
                data-frames='[{"delay":220,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['275','275','171','120']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['270','270','261','190']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-7"
                style={{ zIndex: 9 }}
              >
                <div
                  className="rs-looped rs-wave"
                  data-angle={0}
                  data-origin="50% 50%"
                  data-radius="15px"
                  data-speed={12}
                >
                  <img
                    alt
                    data-hh="['67px','67px','67px','67px']"
                    data-no-retina
                    data-ww="['68px','68px','68px','68px']"
                    src="../static/assets/img/webp/shape-4.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 6 */}
              <div
                className="tp-caption   tp-resizeme"
                data-frames='[{"delay":190,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['413','413','268','204']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['-205','-205','-259','-145']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-8"
                style={{ zIndex: 10 }}
              >
                <div
                  className="rs-looped rs-rotate"
                  data-easing="Power0.easeIn"
                  data-enddeg={360}
                  data-origin="50% 50%"
                  data-speed={15}
                  data-startdeg={0}
                >
                  <img
                    alt
                    data-hh="['69px','69px','69px','69px']"
                    data-no-retina
                    data-ww="['67px','67px','67px','67px']"
                    src="../static/assets/img/webp/shape-3.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 7 */}
              <div
                className="tp-caption   tp-resizeme"
                data-frames='[{"delay":190,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['436','436','259','145']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['204','204','96','73']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['left','left','left','left']"
                data-y="['top','top','top','top']"
                id="slide-43-layer-9"
                style={{ zIndex: 11 }}
              >
                <div
                  className="rs-looped rs-pendulum"
                  data-easing
                  data-enddeg={20}
                  data-origin="50% 50%"
                  data-speed={12}
                  data-startdeg={-20}
                >
                  <img
                    alt
                    data-hh="['52px','52px','52px','52px']"
                    data-no-retina
                    data-ww="['51px','51px','51px','51px']"
                    src="../static/assets/img/webp/shape-1.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 8 */}
              <div
                className="tp-caption   tp-resizeme"
                data-basealign="slide"
                data-frames='[{"delay":170,"speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['39','39','94','58']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-voffset="['109','109','64','69']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['right','right','right','right']"
                data-y="['top','top','top','top']"
                id="slide-43-layer-10"
                style={{ zIndex: 12 }}
              >
                <div
                  className="rs-looped rs-slideloop"
                  data-easing
                  data-speed={2}
                  data-xe={15}
                  data-xs={0}
                  data-ye={0}
                  data-ys={0}
                >
                  <img
                    alt
                    data-hh="['14px','14px','14px','14px']"
                    data-no-retina
                    data-ww="['50px','50px','50px','50px']"
                    src="../static/assets/img/webp/shape-2.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 9 */}
              <div
                className="tp-caption tp-resizeme gradient-text1"
                data-fontsize="['70','65','60','50']"
                data-frames='[{"delay":660,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['320','250','0','0']"
                data-lineheight="['80','75','70','60']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['left','left','center','center']"
                data-type="text"
                data-voffset="['-125','-125','-125','-110']"
                data-whitespace="nowrap"
                data-width="['650','650','600','500']"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-13"
                style={{
                  zIndex: 13,
                  minWidth: 650,
                  maxWidth: 650,
                  whiteSpace: "nowrap",
                  fontSize: 70,
                  lineHeight: 80,
                  fontWeight: 800,
                  color: "#ffffff",
                  letterSpacing: 0,
                  fontFamily: "Montserrat"
                }}
              >
                Maximize Revenue
              </div>
              {/* LAYER NR. 10 */}
              <div
                className="tp-caption   tp-resizeme"
                data-fontsize="['70','65','60','50']"
                data-frames='[{"delay":1840,"split":"chars","splitdelay":0.1,"speed":1000,"split_direction":"forward","frame":"0","from":"sX:0.8;sY:0.8;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
                data-height="none"
                data-hoffset="['320','250','0','0']"
                data-lineheight="['70','75','70','60']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['left','left','center','center']"
                data-type="text"
                data-voffset="['-40','-40','-40','-40']"
                data-whitespace="nowrap"
                data-width="['650','650','600','500']"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-11"
                style={{
                  zIndex: 14,
                  minWidth: 650,
                  maxWidth: 650,
                  whiteSpace: "nowrap",
                  fontSize: 70,
                  lineHeight: 70,
                  fontWeight: 700,
                  color: "#ffffff",
                  letterSpacing: 0,
                  fontFamily: "Montserrat"
                }}
              >
                from your ads
              </div>
              {/* LAYER NR. 11 */}
              <div
                className="tp-caption   tp-resizeme"
                data-fontsize="['20','20','18','17']"
                data-frames='[{"delay":2360,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['325','210','0','0']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['left','left','center','center']"
                data-type="text"
                data-voffset="['80','75','65','57']"
                data-whitespace="normal"
                data-width="['651','550','600','500']"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-14"
                style={{
                  zIndex: 15,
                  minWidth: 651,
                  maxWidth: 651,
                  whiteSpace: "normal",
                  fontSize: 20,
                  lineHeight: 30,
                  fontWeight: 300,
                  color: "#ffffff",
                  letterSpacing: 0,
                  fontFamily: "Roboto"
                }}
              >
                Delivering your brand story to the world. Through our innovative marketing tools.
              </div>
              {/* LAYER NR. 12 */}
              <div
                className="tp-caption   tp-resizeme"
                data-frames='[{"delay":2970,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['325','260','0','0']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['left','left','center','center']"
                data-type="text"
                data-voffset="['200','186','176','156']"
                data-whitespace="nowrap"
                data-width="['650','650','600','500']"
                data-x="['center','center','center','center']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-15"
                style={{ zIndex: 16, whiteSpace: "nowrap" }}
              >
                <a
                  className="btn btn-slider btn-rounded btn-blue btn-hvr-white"
                  href={homePage.website}
                  data-animation-duration={500} 
                  data-fancybox 
                  data-src="#animatedModal"
                >
                  Contact us
                  <div className="btn-hvr-setting">
                    <ul className="btn-hvr-setting-inner">
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                    </ul>
                  </div>
                </a>
              </div>
              {/* LAYER NR. 13 */}
              <div
                className="tp-caption   tp-resizeme"
                data-frames='[{"delay":990,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['0','0','-412','-412']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="image"
                data-visibility="['on','on','off','off']"
                data-voffset="['-1','-1','72','72']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['left','left','left','left']"
                data-y="['middle','middle','middle','middle']"
                id="slide-43-layer-16"
                style={{ zIndex: 17 }}
              >
                <div
                  className="rs-looped rs-slideloop"
                  data-easing
                  data-speed={5}
                  data-xe={15}
                  data-xs={-10}
                  data-ye={0}
                  data-ys={0}
                >
                  <img
                    alt
                    data-hh="['604px','510','510','510']"
                    data-no-retina
                    data-ww="['434px','350','350','350']"
                    src="../static/assets/img/webp/vector-art-1.webp"
                  />
                </div>
              </div>
              {/* LAYER NR. 14 */}
              <div
                className="tp-caption tp-resizeme hide-cursor"
                data-actions='[{"event":"click","action":"scrollbelow","offset":"-50px","delay":"","speed":"1200","ease":"Power3.easeInOut"}]'
                data-fontsize="['20','20','20','17']"
                data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                data-height="none"
                data-hoffset="['0','0','0','0']"
                data-paddingbottom="[0,0,0,0]"
                data-paddingleft="[0,0,0,0]"
                data-paddingright="[0,0,0,0]"
                data-paddingtop="[0,0,0,0]"
                data-responsive_offset="on"
                data-textalign="['inherit','inherit','inherit','inherit']"
                data-type="text"
                data-voffset="['25','25','5','5']"
                data-whitespace="nowrap"
                data-width="none"
                data-x="['center','center','center','center']"
                data-y="['bottom','bottom','bottom','bottom']"
                id="slide-43-layer-17"
                style={{
                  zIndex: 18,
                  whiteSpace: "nowrap",
                  cursor: "pointer",
                  fontSize: 20,
                  lineHeight: 30,
                  fontWeight: 400,
                  color: "#ffffff",
                  letterSpacing: 0,
                  fontFamily: "Montserrat"
                }}
              >
                Scroll Down <i className="ml-2 fas fa-long-arrow-alt-down" />
              </div>
            </li>
          </ul>
          <div
            className="tp-bannertimer tp-bottom"
            style={{ visibility: "hidden !important" }}
          />
        </div>
      </div>
      {/* END REVOLUTION SLIDER */}
    </section>
    {/*Slider End*/}
    {/*About Us*/}
    <section className="pb-0" id="about-us">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 wow fadeInRight">
            <div className="half-img mt-5 pt-4 mt-lg-0 pt-lg-0">
              <img alt="vector" src="../static/assets/img/webp/vector-art-2.webp" />
            </div>
          </div>
          <div className="col-lg-6 wow fadeInLeft">
            <div className="heading-area">
              <span className="sub-title">We are a Creative Digital Agency</span>
              <h2 className="title">
                About <span className="alt-color">Us</span>
              </h2>
              <h2 className="title">
                We are making{" "}
                <span className="alt-color js-rotating">design, ideas</span>{" "}
                better for everyone
              </h2>
              <p className="para">
              We are a group of proffesionals who drives a unique way to advertise, specialize in 
              digital marketing and branding. Giving the best result for your audience through technology 
              and smart distribution content.
              </p>
              <Link as="./aboutus/aboutUsContent" href="./aboutus/">
                <a
                  className="btn btn-large btn-rounded btn-pink btn-hvr-blue mt-3"
                >
                  Check Who We Are
                  <div className="btn-hvr-setting">
                    <ul className="btn-hvr-setting-inner">
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                      <li className="btn-hvr-effect" />
                    </ul>
                  </div>
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*About Us End*/}
    {/*Awesome Service Start*/}
    <section className="text-center" id="team">
      <div className="container">
        {/*Row*/}
        <div className="row">
          <div className="col-md-12">
            <div className="heading-area mx-570 mb-5">
              <h4 className="font-weight-bold">Awesome Service</h4>
              <h2 className="title">
                We have some{" "}
                <span className="alt-color js-rotating">great, ideal</span>{" "}
                talented staff
              </h2>
              {/* <p className="para">
                Oxxo is a tight knit team of independent thinkers sharing a culture that questions and challenges; we work quickly to strategise, conceptualise and deliver well thought through solutions.
              </p> */}
            </div>
          </div>
        </div>
        {/*Row*/}
        <div className="row wow fadeInUp">
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/web & apps development 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">UI/UX</h4>
                  <h4 className="news-title">Design</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/web & apps development 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">Web & Mobile Development</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/sosmed optimization 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">Social Media Optimization</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
        </div>
        {/*Row*/}
        <div className="row wow fadeInUp">
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/digital marketing 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">Digital</h4>
                  <h4 className="news-title">Marketing</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/search engine optimization 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">Search Engine Optimization</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
          {/*Service Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <div className="news-text-box">
                <div className="author d-flex align-items-center">
                  <img style={{borderRadius: "unset"}} alt="image" className="author-img bg-white" src="../static/assets/img/webp/production video 1.webp" />
                </div>
                <a href="#">
                  <h4 className="news-title">Production</h4>
                  <h4 className="news-title">Video</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Awesome Service End*/}
  
    {/*Creating ideas Start*/}
    <section className="pb-0" id="about-us">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 wow fadeInLeft">
            <div className="heading-area">
              <h2 className="title">
                Creating{" "}
                <span className="alt-color js-rotating">Ideas, Building</span>{" "}
                Brand that usefull to people
              </h2>
              <p className="para">
              We are a group of proffesionals who drives a unique way to advertise, specialize in 
              digital marketing and branding. Giving the best result for your audience through technology 
              and smart distribution content.
              </p>
  
              <div className="news-item">
                <div className="news-text-box">
                  <div className="row d-flex align-items-center">
                    <div className="col-3">
                      <div className="author d-flex">
                        <img alt="image" className="author-img bg-white"
                          src="../static/assets/img/webp/Group 47.webp" />
                      </div>
                    </div>
                    <div className="col-9">
                      <a href="#">
                        <h4 className="news-title">Powerful Design</h4>
                      </a>
                      <p className="para">
                        Varius adipiscing scelerisque vel suscipit.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
  
              <div className="news-item">
                <div className="news-text-box">
                  <div className="row d-flex align-items-center">
                    <div className="col-3">
                      <div className="author d-flex">
                        <img alt="image" className="author-img bg-white"
                          src="../static/assets/img/webp/Group 48.webp" />
                      </div>
                    </div>
                    <div className="col-9">
                      <a href="#">
                        <h4 className="news-title">Maximum Research</h4>
                      </a>
                      <p className="para">
                        Varius adipiscing scelerisque vel suscipit.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
  
              <div className="news-item">
                <div className="news-text-box">
                  <div className="row d-flex align-items-center">
                    <div className="col-3">
                      <div className="author d-flex">
                        <img alt="image" className="author-img bg-white"
                          src="../static/assets/img/Group 47.png" />
                      </div>
                    </div>
                    <div className="col-9">
                      <a href="#">
                        <h4 className="news-title">Perfect Code</h4>
                      </a>
                      <p className="para">
                        Varius adipiscing scelerisque vel suscipit.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
  
            </div>
          </div>
          <div className="col-lg-6 wow fadeInRight">
            <div className="half-img mt-5 pt-4 mt-lg-0 pt-lg-0">
              <img alt="vector" src="../static/assets/img/webp/vector-art-5.webp" />
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Creating ideas End*/}
  
    {/*Services Start*/}
    <section className="text-center" id="services">
      <div className="container">
        {/*Row*/}
        <div className="row">
          <div className="col-md-12">
            <div className="heading-area mx-570 mb-5">
              <h2 className="title">
                How Does Our System Function
              </h2>
              <p className="para">
              Our service is reliable, flexible and scalable. We help you improve your product.
              </p>
            </div>
          </div>
        </div>
        {/*Row*/}
        <div className="row">
          <div className="col-md-3">
            <div className="process-wrapp">
              <span className="pro-step blue mb-3">
                <i aria-hidden="true" className="fas fa-lightbulb" />
              </span>
              <h3 className="title">
                Create Idea
              </h3>
              <h4 className="service-heading">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet neque.
              </h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="process-wrapp">
              <span className="pro-step midnight mb-3">
                <i aria-hidden="true" className="fas fa-bullseye" />
              </span>
              <h3 className="title">
                Make A Plan
              </h3>
              <h4 className="service-heading">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet neque.
              </h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="process-wrapp">
              <span className="pro-step purple mb-3">
                <i aria-hidden="true" className="fas fa-laptop-code" />
              </span>
              <h3 className="title">
                Design & Code
              </h3>
              <h4 className="service-heading">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet neque.
              </h4>
            </div>
          </div>
          <div className="col-md-3">
            <div className="process-wrapp">
              <span className="pro-step pink mb-3">
                <i aria-hidden="true" className="fas fa-rocket" />
              </span>
              <h3 className="title">
                Launching
              </h3>
              <h4 className="service-heading">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet neque.
              </h4>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Services End*/}
  
    {/*Project and Partnr Start*/}
    <section id="about-us">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 wow fadeInRight">
            <div className="half-img mt-5 pt-4 mt-lg-0 pt-lg-0">
              <img alt="vector" src="../static/assets/img/webp/vector-art-6.webp" />
            </div>
          </div>
          <div className="col-lg-6 wow fadeInLeft">
            <div className="heading-area">
              <h2 className="title">
                We completed several projects yearly succesfully & Counting
              </h2>
              <p className="para">
              We are a group of proffesionals who drives a unique way to advertise, specialize in 
              digital marketing and branding. Giving the best result for your audience through technology 
              and smart distribution content.
              </p>
  
              <a href="#portfolio" className="btn btn-large btn-rounded btn-pink btn-hvr-blue mt-2 mb-4 link scroll">
                View all Projects
                <div className="btn-hvr-setting">
                  <ul className="btn-hvr-setting-inner">
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                    <li className="btn-hvr-effect" />
                  </ul>
                </div>
              </a>
  
              <div className="row mb-5">
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_atwork 1.webp" />
                  </div>
                </div>
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_autodesk 1.webp" />
                  </div>
                </div>
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_informatica 1.webp" />
                  </div>
                </div>
              </div>
  
              <div className="row mb-5">
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_intel 1.webp" />
                  </div>
                </div>
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_ohp 1.webp" />
                  </div>
                </div>
                <div className="col-4">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_usakti 1.webp" />
                  </div>
                </div>
              </div>
  
              <div className="row">
                <div className="col-6">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_smi 1.webp" />
                  </div>
                </div>
                <div className="col-6">
                  {/*Item*/}
                  <div className="logo-item">
                    <img alt="client-logo" src="../static/assets/img/webp/logo_sukabumi 1.webp" />
                  </div>
                </div>
              </div>
  
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Project and Partnr End*/}
  
    {/*Counters Start*/}
    <section className="gradient-bg2" id="counters">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 pl-lg-4 order-lg-2 wow fadeInRight">
            <div className="heading-area">
              <span className="sub-title text-white">We are Oxxo company</span>
              <h2 className="title text-white">
                We have done some great{" "}
                <span className="js-rotating">stuff, idea</span>.
              </h2>
              <p className="para text-white">
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form, by
                injected.
              </p>
            </div>
            <ul className="counter-list list-unstyled">
              <li className="counter-item">
                <i aria-hidden="true" className="fas fa-users text-white" />
                <h6 className="counter-number text-white">
                  <span className="count">500</span>+
                </h6>
                <p className="sub-title text-white">Happy Clients</p>
              </li>
              <li className="counter-item">
                <i aria-hidden="true" className="fas fa-list-alt text-white" />
                <h6 className="counter-number text-white">
                  <span className="count">1074</span>+
                </h6>
                <p className="sub-title text-white">Lines Of Code</p>
              </li>
              <li className="counter-item">
                <i aria-hidden="true" className="fas fa-award text-white" />
                <h6 className="counter-number text-white">
                  <span className="count">600</span>+
                </h6>
                <p className="sub-title text-white">Project Completed</p>
              </li>
            </ul>
          </div>
          <div className="col-lg-6 wow fadeInLeft">
            <div className="half-img mt-5 pt-4 mt-lg-0 pt-lg-0">
              <img alt="vector" src="../static/assets/img/webp/vector-art-3.webp" />
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Counters End*/}
    {/*Blog Start*/}
    <section className="bg-light" id="blog">
      <div className="container">
        {/*Row*/}
        <div className="row">
          <div className="col-md-12 text-center">
            <div className="heading-area mx-570 pb-lg-5 mb-5">
              <span className="sub-title">We are Oxxo company</span>
              <h2 className="title mb-0">
                Our{" "}
                <span className="alt-color js-rotating">
                  latest blogs,recent news
                </span>{" "}
                will keep everyone updated
              </h2>
            </div>
          </div>
        </div>
        {/*Row*/}
        <div className="row wow fadeInUp">
          {/*News Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <img
                alt="image"
                className="news-img"
                src="../static/assets/img/webp/blog-news-1.webp"
              />
              <div className="news-text-box">
                <span className="date main-color">October 29, 2020</span>
                <a href="../static/assets/blog-list.html">
                  <h4 className="news-title">Web design is fun</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
                <a
                  className="author d-flex align-items-center"
                  href={homePage.website}
                >
                  <img
                    alt="image"
                    className="author-img bg-blue"
                    src="../static/assets/img/avatar-1.png"
                  />
                  <h5 className="author-name">Hena Sword</h5>
                </a>
              </div>
            </div>
          </div>
          {/*News Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <img
                alt="image"
                className="news-img"
                src="../static/assets/img/webp/blog-news-2.webp"
              />
              <div className="news-text-box">
                <span className="date main-color">October 29, 2020</span>
                <a href="../static/assets/blog-list.html">
                  <h4 className="news-title">Future of websites</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
                <a
                  className="author d-flex align-items-center"
                  href={homePage.website}
                >
                  <img
                    alt="image"
                    className="author-img bg-purple"
                    src="../static/assets/img/avatar-2.png"
                  />
                  <h5 className="author-name">David Villas</h5>
                </a>
              </div>
            </div>
          </div>
          {/*News Item*/}
          <div className="col-lg-4">
            <div className="news-item">
              <img
                alt="image"
                className="news-img"
                src="../static/assets/img/webp/blog-news-3.webp"
              />
              <div className="news-text-box">
                <span className="date main-color">October 29, 2020</span>
                <a href={homePage.website}>
                  <h4 className="news-title">Digital marketing</h4>
                </a>
                <p className="para">
                  Lorem ipsum dolor sit amet consectetur adipiscing elit ipsum
                  dolor sit am...
                </p>
                <a
                  className="author d-flex align-items-center"
                  href={homePage.website}
                >
                  <img
                    alt="image"
                    className="author-img bg-pink"
                    src="../static/assets/img/avatar-5.png"
                  />
                  <h5 className="author-name">Jhon Walker</h5>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Blog End*/}
    {/*Client Map*/}
    <section className="gradient-bg2" id="client">
      <h2 className="d-none">hidden</h2>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            {/*Client Slider*/}
            <div className="owl-carousel partners-slider">
              {/*Item*/}
              <div className="logo-item">
                <img alt="client-logo" src="../static/assets/img/client-1.png" />
              </div>
              {/*Item*/}
              <div className="logo-item">
                <img alt="client-logo" src="../static/assets/img/client-2.png" />
              </div>
              {/*Item*/}
              <div className="logo-item">
                <img alt="client-logo" src="../static/assets/img/client-3.png" />
              </div>
              {/*Item*/}
              <div className="logo-item">
                <img alt="client-logo" src="../static/assets/img/client-4.png" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/*Client End*/}
    {/*Map Start*/}
    <div className="p-0 gradient-bg2 map-area">
      <div className="container">
        {/*Map Initialize*/}
        <div className="full-map" id="map" />
      </div>
    </div>
    {/*Map End*/}
    {/*Contact Start*/}
    <section className="contact-us" id="contact">
      <div className="container">
        <div className="row align-items-top">
          <div className="col-lg-5 order-lg-2 wow fadeInRight">
            <div className="contact-detail">
              <div className="contact-dots" data-dots />
              {/*Heading*/}
              <div className="heading-area pb-5">
                <h2 className="title mt-0 pb-1">Our Location</h2>
                <p className="para">
                  There are many variations of passages of Lorem Ipsum available,
                  but the majority have suffered .
                </p>
              </div>
              {/*Address*/}
              <ul className="address list-unstyled">
                <li>
                  <span className="address-icon gradient-text2">
                    <i aria-hidden="true" className="fas fa-paper-plane" />
                  </span>
                  <span className="address-text">
                    <a className="mr-3 alt-color" href={homePage.website}>
                    hello@oxxo.co.id
                    </a>
                  </span>
                </li>
                <li>
                  <span className="address-icon gradient-text2">
                    <i aria-hidden="true" className="fas fa-phone-volume" />
                  </span>
                  <span className="address-text">
                    <a className="mr-3" href={homePage.website}>
                      +1 631 1234 5678
                    </a>
                    <a href={homePage.website}>/ 0811 2511 993</a>
                  </span>
                </li>
                <li>
                  <span className="address-icon gradient-text2">
                    <i aria-hidden="true" className="fas fa-map-marker-alt" />
                  </span>
                  <span className="address-text">
                  Menara Mandiri 1, Jl. Jend. Sudirman Kav. 54-55, Senayan, Kby. Baru, Jakarta Selatan 12190
                  </span>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-lg-7 mt-4 pt-3 mt-lg-0 pt-lg-0 wow fadeInLeft">
            {/*Heading*/}
            <div className="heading-area pb-2">
              <h2 className="title mt-0">Lets Start Your <span className="alt-color">Projects</span></h2>
            </div>
            {/*Contact Form*/}
            <form onSubmit={this.handleSubmit} className="contact-form">
              <div className="row">
                {/*Result*/}
                <div className="col-12" id="result" />
                {/*Left Column*/}
                <div className="col-md-5">
                  <div className="form-group">
                    <input
                      onChange={(event) => this.setState({name: event.target.value})}
                      className="form-control"
                      name="name"
                      placeholder="Name"
                      required
                      type="text"
                    />
                  </div>
                  <div className="form-group">
                    <input
                      onChange={(event) => this.setState({email: event.target.value})}
                      className="form-control"
                      name="email"
                      placeholder="Email"
                      required
                      type="email"
                    />
                    </div>
                  <div className="form-group">
                    <input
                      onChange={(event) => this.setState({phone: event.target.value})}
                      className="form-control"
                      name="phone"
                      placeholder="Phone"
                      required
                      type="number"
                    />
                    </div>
                </div>
                {/*Right Column*/}
                <div className="col-md-7">
                  <div className="form-group">
                    <textarea
                      className="form-control"
                      onChange={(event) => this.setState({content: event.target.value})}
                      name="content"
                      placeholder="Message"
                    />
                  </div>
                </div>
                {/*Button*/}
                <div className="col-md-12">
                  <button
                    className="btn btn-large btn-rounded btn-purple btn-hvr-blue d-block mt-4"
                    type="submit"
                  >
                    Send Message
                    <div className="btn-hvr-setting">
                      <ul className="btn-hvr-setting-inner">
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                        <li className="btn-hvr-effect" />
                      </ul>
                    </div>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    {/*Contact End*/}
    {/*Footer Start*/}
    <footer className="footer-style-1 bg-light">
      <div className="container">
        <div className="row align-items-center">
          {/*Social*/}
          <div className="col-md-6">
            <div className="footer-social">
              <ul className="list-unstyled">
                <li>
                  <a className="wow fadeInUp" href="https://www.facebook.com/oxxoagency" target="_blank">
                    <i aria-hidden="true" className="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a className="wow fadeInDown" href="https://www.linkedin.com/company/oxxo-digital-agency/" target="_blank">
                    <i aria-hidden="true" className="fab fa-linkedin-in" />
                  </a>
                </li>
                <li>
                  <a className="wow fadeInUp" href="https://www.instagram.com/oxxo.agency/" target="_blank">
                    <i aria-hidden="true" className="fab fa-instagram" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/*Text*/}
          <div className="col-md-6 text-md-right">
            <p className="company-about fadeIn">
              Copyright © 2019 <a href={homePage.website}>OXXO</a> . All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </footer>
    {/*Footer End*/}
    {/*Animated Cursor*/}
    <div id="aimated-cursor">
      <div id="cursor">
        <div id="cursor-loader" />
      </div>
    </div>
    {/*Animated Cursor End*/}
    {/*Scroll Top Start*/}
    <span className="scroll-top-arrow">
      <i className="fas fa-angle-up" />
    </span>
    {/*Scroll Top End*/}
  </div>
      </LayoutMain>
    )

  }
}

export default Homepage;